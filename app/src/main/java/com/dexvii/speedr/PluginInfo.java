package com.dexvii.speedr;


import android.content.Context;
import android.net.Uri;

import org.geometerplus.android.fbreader.api.PluginApi;

import java.util.Collections;
import java.util.List;

public class PluginInfo extends PluginApi.PluginInfo {
	@Override
	protected List<PluginApi.ActionInfo> implementedActions(Context context) {
        String appName = SpeedrApplication.getInstance().getResources().getString(R.string.app_name);
		return Collections.<PluginApi.ActionInfo>singletonList(new PluginApi.MenuActionInfo(
			Uri.parse("http://data.fbreader.org/plugin/speedread"),
				appName,
			Integer.MAX_VALUE
		));
	}
}
