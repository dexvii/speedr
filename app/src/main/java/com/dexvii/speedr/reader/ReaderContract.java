package com.dexvii.speedr.reader;

public class ReaderContract {

    public interface View {

        void dismiss();

        void showWord(String word);

        void setPlaying();

        void setStopped();

        void setPresenter(Presenter presenter);

    }

    public interface Presenter {

        void playStopToggle();

        void setWPM(int wpm);

        void start();

        void stop();

    }
}
