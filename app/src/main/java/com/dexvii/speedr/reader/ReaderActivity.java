package com.dexvii.speedr.reader;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.dexvii.speedr.R;

public class ReaderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ReaderFragment readerFragment = (ReaderFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        if (readerFragment == null) {
            readerFragment = ReaderFragment.newInstance();

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(R.id.contentFrame, readerFragment);
            transaction.commit();
        }

        new ReaderPresenter(this, ReaderFragment.DEFAULT_WPM, readerFragment);
    }
}
