package com.dexvii.speedr.reader;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.dexvii.speedr.R;
import com.dexvii.speedr.SpeedrApplication;
import com.dexvii.speedr.strings.Strings;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mikepenz.aboutlibraries.Libs;
import com.mikepenz.aboutlibraries.LibsBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReaderFragment extends Fragment implements ReaderContract.View {

    private static final String ANALYTICS_ON_RESUME = "on_resume";
    private static final String ANALYTICS_ON_PAUSE = "on_pause";
    private static final String ANALYTICS_MENU_ABOUT = "menu_about";
    private static final String ANALYTICS_DISMISS = "dismiss";
    private static final String ANALYTICS_SET_PLAYING = "set_playing";
    private static final String ANALYTICS_SET_STOPPED = "set_stopped";
    private static final String ANALYTICS_SET_WPM = "set_wpm_";

    public static final int DEFAULT_WPM = 250;
    FirebaseAnalytics mFirebaseAnalytics;

    ReaderContract.Presenter mPresenter;

    Map<String, Integer> mWpms;

    TextView textViewSpeedWord;
    ImageButton imageButtonPlayStop;
    Spinner spinnerWpm;


    public static ReaderFragment newInstance() {

        Bundle args = new Bundle();

        ReaderFragment fragment = new ReaderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ReaderFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reader, container, false);

        mFirebaseAnalytics = SpeedrApplication.getInstance().getFirebaseAnalytics();

        textViewSpeedWord = (TextView) view.findViewById(R.id.text_view_speed_word);
        imageButtonPlayStop = (ImageButton) view.findViewById(R.id.button_playstop);
        spinnerWpm = (Spinner) view.findViewById(R.id.spinner_wpm);

        setUpButtonListeners();
        setUpWpmSpinner();

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_reader_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_speedr_about:
                mFirebaseAnalytics.logEvent(ANALYTICS_MENU_ABOUT, null);
                startAboutApplicationFragment();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        mFirebaseAnalytics.logEvent(ANALYTICS_ON_RESUME, null);

        mPresenter.start();
    }

    @Override
    public void onPause() {
        if (mPresenter != null) {
            mPresenter.stop();
        }

        mFirebaseAnalytics.logEvent(ANALYTICS_ON_PAUSE, null);

        super.onPause();
    }

    @Override
    public void dismiss() {
        mFirebaseAnalytics.logEvent(ANALYTICS_DISMISS, null);
        getActivity().onBackPressed();
    }

    @Override
    public void setPlaying() {
        mFirebaseAnalytics.logEvent(ANALYTICS_SET_PLAYING, null);
        imageButtonPlayStop.setImageResource(R.drawable.ic_stop_black_48dp);
    }

    @Override
    public void setStopped() {
        mFirebaseAnalytics.logEvent(ANALYTICS_SET_STOPPED, null);
        imageButtonPlayStop.setImageResource(R.drawable.ic_play_arrow_black_48dp);
    }

    @Override
    public void showWord(String word) {
        if (word != null) {
            textViewSpeedWord.setText(word);
        } else {
            textViewSpeedWord.setText(R.string.book_eof);
        }
    }

    @Override
    public void setPresenter(ReaderContract.Presenter presenter) {
        mPresenter = presenter;
    }

    private void setUpButtonListeners() {
        imageButtonPlayStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.playStopToggle();
            }
        });
    }

    private int setUpWpmSpinner() {

        mWpms = new HashMap<>();
        mWpms.put("700 wpm", 700);
        mWpms.put("650 wpm", 650);
        mWpms.put("600 wpm", 600);
        mWpms.put("550 wpm", 550);
        mWpms.put("500 wpm", 500);
        mWpms.put("450 wpm", 450);
        mWpms.put("400 wpm", 400);
        mWpms.put("350 wpm", 350);
        mWpms.put("300 wpm", 300);
        mWpms.put("250 wpm", 250); // default
        mWpms.put("200 wpm", 200);
        mWpms.put("150 wpm", 150);
        mWpms.put("100 wpm", 100);

        final List<String> wpmKeys = new ArrayList<>(mWpms.keySet());
        Collections.sort(wpmKeys, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return t1.compareTo(s);
            }
        });

        ArrayAdapter<String> arrayAdapter =
                new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, wpmKeys);
        spinnerWpm.setAdapter(arrayAdapter);

        spinnerWpm.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                String key = wpmKeys.get(pos);
                int wpm = mWpms.get(key);
                mFirebaseAnalytics.logEvent(ANALYTICS_SET_WPM + wpm, null);
                mPresenter.setWPM(wpm);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        int spinnerIndex = wpmKeys.indexOf(DEFAULT_WPM + " wpm");
        spinnerWpm.setSelection(spinnerIndex);
        String defaultKey = (String) spinnerWpm.getSelectedItem();
        return mWpms.get(defaultKey);
    }

    private void startAboutApplicationFragment() {
        new LibsBuilder()
                .withFields(R.string.class.getFields())
                .withActivityStyle(Libs.ActivityStyle.LIGHT_DARK_TOOLBAR)
                .withActivityTitle(getString(R.string.settings_about))
                .withAboutIconShown(true)
                .withAboutDescription(getString(R.string.settings_about_description))
                .withAboutVersionShown(true)
                .withAboutSpecial1(getString(R.string.button_license_title))
                .withAboutSpecial1Description(Strings.DISCLAIMER)
                .withLicenseShown(true)
                .withLibraries("FBreaderAPI")
                .withAboutAppName(getString(R.string.app_name))
                .start(getActivity());
    }

}
