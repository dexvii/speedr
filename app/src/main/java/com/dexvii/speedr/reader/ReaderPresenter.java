package com.dexvii.speedr.reader;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.geometerplus.android.fbreader.api.ApiClientImplementation;
import org.geometerplus.android.fbreader.api.ApiException;
import org.geometerplus.android.fbreader.api.TextPosition;

import java.util.List;

public class ReaderPresenter implements ReaderContract.Presenter, ApiClientImplementation.ConnectionListener {

    public static final int PARAGRAPH_SLEEP_TIME = 500;

    private ReaderContract.View mView;

    private ApiClientImplementation mFbReaderApi;
    private int mWpm;
    private int mWordDelay;
    private int mCurrentParagraphIndex;
    private int mCurrentParagraphNumber;
    private AsyncTask mCurrentSpeedReadTask;

    public ReaderPresenter(Context ctx, int wpm, ReaderContract.View view) {
        mView = view;
        mWpm = wpm;
        mFbReaderApi = new ApiClientImplementation(ctx, this);

        mView.setPresenter(this);
    }

    @Override
    public void playStopToggle() {
        boolean isPlaying = mCurrentSpeedReadTask != null &&
                mCurrentSpeedReadTask.getStatus() == AsyncTask.Status.RUNNING;
        if (isPlaying) {
            stopPlay();
        } else {
            startPlay();
        }
    }

    @Override
    public void setWPM(int wpm) {
        mWpm = wpm;
        mWordDelay = calculateWordDelay(mWpm);
        Log.i(getClass().getName(), "Set wpm: " + wpm + " delay: " + mWordDelay);
    }

    @Override
    public void start() {
        Log.i(getClass().getName(), "Connect to fbreader");
        mFbReaderApi.connect();
    }

    @Override
    public void stop() {
        stopPlay();
        mFbReaderApi.disconnect();
    }

    @Override
    public void onConnected() {
        Log.i(getClass().getName(), "Connected to fbreader");

        try {
            mCurrentParagraphIndex = mFbReaderApi.getPageStart().ParagraphIndex;
            mCurrentParagraphNumber = mFbReaderApi.getParagraphsNumber();
            stopPlay();
        } catch (ApiException e) {
            mView.dismiss();
        }
    }

    private void startPlay() {
        mView.setPlaying();
        speedRead();
    }

    private void stopPlay() {
        if (mCurrentSpeedReadTask != null) {
            mCurrentSpeedReadTask.cancel(true);
        }

        mView.setStopped();
    }

    public void speedRead()  {
        if (mCurrentParagraphIndex < mCurrentParagraphNumber) {
            try {
                highlightParagraph(mCurrentParagraphIndex);
                setPage(mCurrentParagraphIndex);
                List<String> words = mFbReaderApi.getParagraphWords(mCurrentParagraphIndex);
                Log.i(getClass().getName(), "Paragraph words: " + words);
                mCurrentSpeedReadTask = new SpeedReadParagraphTask()
                        .execute(words.toArray(new String[words.size()]));

            } catch (ApiException e) {
                mView.dismiss();
            }
        } else {
            mView.showWord(null);
        }
    }

    private void highlightParagraph(int paragrphIndex) throws ApiException {
        mFbReaderApi.highlightArea(
                new TextPosition(paragrphIndex, 0, 0),
                new TextPosition(paragrphIndex, Integer.MAX_VALUE, 0));
    }

    private void setPage(int paragrphIndex) throws ApiException {
        mFbReaderApi.setPageStart(new TextPosition(paragrphIndex, 0, 0));
    }

    private int calculateWordDelay(int wpm) {
        return (60*1000)/wpm; // 60000 ms per minute / wpm = delay between each word
    }

    private void speedReadParagraphTaskDone() {
        mCurrentParagraphIndex++;
        speedRead();
    }

    private class SpeedReadParagraphTask extends AsyncTask<String, String, String> {
        protected String doInBackground(String... words) {
            try {
                Thread.sleep(PARAGRAPH_SLEEP_TIME);
                for (String word : words) {
                    Thread.sleep(mWordDelay);
                    publishProgress(word);
                    if (isCancelled()) break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return "";
        }

        protected void onProgressUpdate(String... word) {
            Log.d(getClass().getName(), "Set word: " + word[0]);
            mView.showWord(word[0]);
        }

        protected void onPostExecute(String s) {
            speedReadParagraphTaskDone();
        }
    }
}
