package com.dexvii.speedr;

import android.app.Application;

import com.google.firebase.analytics.FirebaseAnalytics;

public class SpeedrApplication extends Application {

    private static SpeedrApplication instance;
    private static FirebaseAnalytics mFirebaseAnalytics;

    public static SpeedrApplication getInstance() {
        return instance;
    }

    public SpeedrApplication() {
        instance = this;
    }

    public FirebaseAnalytics getFirebaseAnalytics() {
        if (mFirebaseAnalytics == null) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
        }

        return mFirebaseAnalytics;
    }

}
